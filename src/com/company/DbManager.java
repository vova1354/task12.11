package com.company;

import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;

import java.util.ArrayList;
import java.util.List;

public class DbManager {

    private static final String USER_NAME = "postgres";
    private static final String PASSWORD = "1111";
    private static final String URL = "jdbc:postgresql://localhost:5432/postgres";

    public Connection getConnection() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(URL, USER_NAME, PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    private void dataSource() {
        try {
            Connection connection = getConnection();
            List<String> values = read("D:\\task.txt");
            for (String s : values) {
                if (s.trim().length() == 0) {
                    continue;
                }
                String[] words = s.split(" ");
                String name = words[0];
                String address = words[0];
                Integer id = Integer.valueOf(words[1]);
                String sqlSelect = "INSERT INTO Person (id , name , address) VALUES (?, ?, ?)";
                PreparedStatement preparedStatement = connection.prepareStatement(sqlSelect);
                preparedStatement.setInt(1, id);
                preparedStatement.setString(2, name);
                preparedStatement.setString(3, address);
                preparedStatement.execute();
                preparedStatement.close();
            }
            connection.close();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    private boolean checkTableExistence(String name) {
        boolean result = true;
        try {
            Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM  " + name);
            preparedStatement.executeQuery();
        } catch (Exception e) {
            System.out.println("Таблицы не существует");
            result = false;
        }
        return result;
    }

    public void checkListTable(Class clazz) {


            Entity annotation = (Entity) clazz.getAnnotation(Entity.class);
            if (annotation == null) {
                return;
            }
            String tableName = annotation.name();
            if (tableName.length() == 0) {
                return;
            }
            if (checkTableExistence(tableName)) {
                System.out.println("Таблица сущестует");
                return;
            }
        createTable(clazz);
    }
//
//    public List<Object> getList(Class clazz) throws SQLException, IllegalAccessException, InstantiationException {
//        List<Object> rezults = new ArrayList<>();
//        Field[] fields = clazz.getDeclaredFields();
//        List<String> names = new ArrayList<>();
//
//
//        for (Field f : fields) {
//            if (f.getAnnotation(Colump.class) != null) {
//                System.out.println("Аннотация над полем " + f.getName());
//                names.add(f.getName());
//            } else {
//                System.out.println("Аннотации НЕТ над полем " + f.getName());
//            }
//        }
//        Connection connection = getConnection();
//        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM  "+clazz.getSimpleName());
//        ResultSet resultSet =  preparedStatement.executeQuery();
//        while (resultSet.next()){
//            Object object = clazz.newInstance();
//            for (Field f :fields){
//
//                f.setAccessible(true);
//                f.set(object,resultSet.getObject(f.getName()));
//            }
//            rezults.add(object);
//            System.out.println(clazz.getSimpleName().toString()+" name = " +resultSet.getString("name"));
////            System.out.println(resultSet.getString("name"));
//
//        }
//        resultSet.close();
//        connection.close();
//        return rezults;
//    }

    private void createTable(Class clazz) {
        Field[] fields = clazz.getDeclaredFields();
        Entity annotation = (Entity) clazz.getAnnotation(Entity.class);
        StringBuilder builder = new StringBuilder("CREATE TABLE " + annotation.name() + " (");
        int i = 0;
        for (Field f : fields) {
            Class c = f.getType();
            if (i > 0) builder.append(", ");
            builder.append(f.getName()).append(" ").append(getType(c));
            i++;
        }
        builder.append(");");
        System.out.println(builder);
        if (builder.length() > 0) {
            if (startCommand(builder.toString())) {
                if (annotation.name().equals("Person")) {
                    dataSource();
                }
            }
        }
    }
    public static List<String> read(String file) {
        List<String> tmplines = new ArrayList<>();
        try {
            tmplines = Files.readAllLines(Paths.get(file), StandardCharsets.UTF_8);
        } catch (IOException e) {
            try {
                tmplines = Files.readAllLines(Paths.get(file), Charset.forName("CP1251"));
            } catch (IOException er) {
                System.out.println("FAIL READ FILE");
                er.printStackTrace();
            }
        }
        return tmplines;
    }

    private String getType(Class clazz) {
        if (clazz.isAssignableFrom(Integer.class)) {
            return "INT";
        }
        if (clazz.isAssignableFrom(Double.class)) {
            return "VARCHAR(255)";
        }
        if (clazz.isAssignableFrom(String.class)) {
            return "VARCHAR(100)";
        }
        return "INT";
    }

    private boolean startCommand(String sql) {
        boolean result = true;
        try {
            Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.execute();
            preparedStatement.close();
            connection.close();
            System.out.println("Таблица создана успешно");
        } catch (Exception e) {
            System.out.println("Не удалось исполнить запрос");
            e.printStackTrace();
            result = false;
        }
        return result;
    }

    }



